FROM prodataninja/ubuntu-python2.7
USER root
RUN mkdir /app
WORKDIR /app

RUN apt-get -y update && \
  apt-get -y dist-upgrade && \
 apt-get -y install git && \
 apt-get -y install apache2 && \
  apt-get -y install python-pip 
#RUN apt-get -y install pymysql 
CMD a2dismod mpm_event
CMD a2enmod mpm_prefork cgi
CMD service apache2 restart
RUN wget "https://s3.amazonaws.com/richbourg-s3/mtwa/web/000-default.conf" && \
wget "https://s3.amazonaws.com/richbourg-s3/mtwa/web/ports.conf"
RUN git clone https://gitlab.com/crest1/devops_test.git
WORKDIR /app/devops_test
CMD service apache2 start
RUN bash install.sh
